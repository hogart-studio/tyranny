(function () {
    'use strict';

    function isGameEnd() {
        if (passage() !== 'Путь') {
            return false;
        }

        const { encounters, currentEncounter } = State.current.variables;
        const currentPassage = encounters[currentEncounter];
        return currentPassage === '04 Итог';
    }

    const achievements = [
        {
            id: 'pure-of-heart',
            title: 'Pure of Heart',
            description: 'Break the vicious circle',
            unlocked: false,
            hidden: true,
            test() {
                const { evil } = State.current.variables;
                if (isGameEnd() && evil === 0) {
                    return true;
                }
            },
        },
        {
            id: 'evil-reincarnate',
            title: 'Evil Reincarnate',
            description: '',
            unlocked: false,
            hidden: true,
            test() {
                const { kind } = State.current.variables;
                if (isGameEnd() && kind === 0) {
                    return true;
                }
            },
        },
        {
            id: 'no-rest-for-the-wicked',
            title: 'No Rest for the Wicked',
            description: '',
            unlocked: false,
            hidden: true,
            test() {
                const { kind, evil, rounds } = State.current.variables;
                if (isGameEnd() && kind < evil && rounds > 1) {
                    return true;
                }
            },
        },
        {
            id: 'perfect-balance',
            title: 'Perfect Balance',
            description: 'Light and Darkness',
            unlocked: false,
            hidden: true,
            test() {
                const { kind, evil } = State.current.variables;
                if (isGameEnd() && kind === evil) {
                    return true;
                }
            },
        },
    ];

    window.game = Object.assign(window.game || {}, {
        achievements,
    });
}());
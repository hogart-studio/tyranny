function getTitle(p) {
    return p.title;
}

function getByTags(...tags) {
    return Story.lookupWith((p) => p.tags.includesAll(tags))
}

window.getEncounters = function getEncounters() {
    return getByTags('encounter').map(getTitle);
};

window.getStages = function getStages() {
    return getByTags('stage').map(getTitle).sort();
};

window.getStoryFlow = function getStoryFlow(encountersPerStage = 3, stages = 4) {
    const stagePassages = getStages();
    const flow = [];
    for (let i = 0; i < stages; i++) {
        const fillers = getByTags('encounter', 'stage-' + i).filter((p) => !flow.includes(p.title)).shuffle();
        flow.push(...fillers.slice(0, 3).map(getTitle));
        flow.push(stagePassages[i]);
    }

    return flow;
};
Config.ui.stowBarInitially = true;
Config.saves.autosave = ['bookmark', 'stage'];

(function detectLangFactory() {
    'use strict';

    const pathNameMatcher = /(-[a-z]{2})?\.html$/;
    function byPath() {
        let lang = 'ru';
        const match = window.location.pathname.match(pathNameMatcher);
        if (match && match[1]) {
            lang = match[1].substr(1);
        }

        return lang;
    }

    window.scUtils = Object.assign(
        window.scUtils || {},
        {
            detectLang: {
                byPath,
            },
        }
    );
}());

const lang = window.scUtils.detectLang.byPath();

// needed for hyphenation and TTS to work properly
document.documentElement.setAttribute('lang', lang);

if (lang === 'ru') {
    (function () {
        l10nStrings = {
            identity: 'игры',
            aborting: 'Идет отмена',
            cancel: 'Отменить',
            close: 'Закрыть',
            ok: 'ОК',

            errorTitle: 'Ошибка',
            errorNonexistentPassage: 'Параграф "{passage}" не существует',
            errorSaveMissingData: 'в сохранении нет необходимых данных. Сохранение было повреждено или загружен неверный файл',
            errorSaveIdMismatch: 'сохранение от другой {identity}',

            _warningIntroLacking: 'Приносим извинения! В вашем браузере отсутствуют либо выключены необходимые функции',
            _warningOutroDegraded: ', так что включен ограниченный режим. Вы можете продолжать, но кое-что может работать некорректно.',
            warningNoWebStorage: '{_warningIntroLacking} (Web Storage API) {_warningOutroDegraded}',
            warningDegraded: '{_warningIntroLacking} (требуемые этой игрой) {_warningOutroDegraded}',

            debugViewTitle: 'Режим отладки',
            debugViewToggle: 'Переключить режим отладки',

            uiBarToggle: 'Открыть/закрыть панель навигации',
            uiBarBackward: 'Назад по истории {identity}',
            uiBarForward: 'Вперед по истории {identity}',
            uiBarJumpto: 'Перейти в определенную точку истории {identity}',

            jumptoTitle: 'Перейти на',
            jumptoTurn: 'Ход',
            jumptoUnavailable: 'В данный момент нет точек для перехода\u2026',

            savesTitle: 'Сохранения',
            savesDisallowed: 'На этом параграфе сохранение запрещено.',
            savesEmptySlot: '— пустой слот —',
            savesIncapable: '{_warningIntroLacking}, так что сохранения невозможны в текущей сессии',
            savesLabelAuto: 'Автосохранение',
            savesLabelDelete: 'Автосохранение',
            savesLabelExport: 'Сохранить на диск\u2026',
            savesLabelImport: 'Загрузить с диска\u2026',
            savesLabelLoad: 'Загрузить',
            savesLabelClear: 'Удалить все',
            savesLabelSave: 'Сохранить',
            savesLabelSlot: 'Слот',
            savesSavedOn: 'Сохранено: ',
            savesUnavailable: 'Слоты сохранения не обнаружены\u2026',
            savesUnknownDate: 'неизвестно',

            settingsTitle: 'Настройки',
            settingsOff: 'Выкл.',
            settingsOn: 'Вкл.',
            settingsReset: 'По умолчанию',

            restartTitle: 'Начать с начала',
            restartPrompt: 'Вы уверены, что хотите начать сначала? Несохраненный прогресс будет утерян.',

            shareTitle: 'Поделиться',

            autoloadTitle: 'Автосохранение',
            autoloadCancel: 'Начать с начала',
            autoloadOk: 'Загрузить сохранение',
            autoloadPrompt: 'Найдено автосохранение. Загрузить его или начать с начала?',

            macroBackText: 'Назад',
            macroReturnText: 'Вернуться',
        }
    }());

    l10nStrings.uiBarNightMode = 'Ночной режим';
    l10nStrings.uiBarAbout = 'Об игре';
    l10nStrings.achievements = 'Достижения';
    l10nStrings.uiFontSize = 'Размер шрифта';

    window.achievementTemplate = (hiddenAchievementsCount, unlockedAchievementsCount) => { /* Pluralizer function (which renders 'And 3 hidden achievements' after the list) */
        const template = unlockedAchievementsCount > 0 ? 'И еще ${amount} ${plural}.' : '${amount} ${plural}.';
        return scUtils.pluralizeFmt(['скрытое достижение', 'скрытых достижения', 'скрытых достижений'], template)(hiddenAchievementsCount);
    }
} else {
    l10nStrings.achievements = 'Achievements';

    window.achievementTemplate = (hiddenAchievementsCount, unlockedAchievementsCount) => { /* Pluralizer function (which renders 'And 3 hidden achievements' after the list) */
        const template = unlockedAchievementsCount > 0 ? 'And ${amount} ${plural}.' : '${amount} ${plural}.';
        return scUtils.pluralizeFmt(['hidden achievement', 'hidden achievements'], template)(hiddenAchievementsCount);
    }
}

(function menuButtonUtil() {
    'use strict';

    // Utility functions to create buttons in dock menu.
    // scUtils.createPassageButton creates button which opens dialog displaying passage with given name.
    // scUtils.createHandlerButton creates button which calls given handler.
    // Both methods return {button, style} objects with jQuery-wrapped references to created elements

    /* globals Story, Dialog */

    // save some DOM references for later use
    const $head = jQuery('head');
    const $menuCore = jQuery('#menu-core');

    function createButton(id, label, onClick) {
        const buttonTemplate = `<li id="${id}"><a>${label}</a></li>`;
        const $button = jQuery(buttonTemplate);

        $button.ariaClick(onClick);
        $button.appendTo($menuCore);

        return $button;
    }

    function createMultiButton(id, mainLabel, labels, onClick) {
        const buttonTemplate = `
            <li id="${id}" class="multiButton">
                ${mainLabel ? `<div class="mainLabel">${mainLabel}</div>` : ''}
                <div class="buttons">
                    ${labels.map(label => `<a>${label}</a>`).join('')}
                </div>
            </li>`;
        const $button = jQuery(buttonTemplate);
        $button.on('click', 'a', (event) => {
            const index = jQuery(event.currentTarget).index();
            onClick(event, index);
        });

        if (jQuery('style#multi-button-style').length === 0) {
            const styles = `
                .multiButton .mainLabel {
                    text-transform: uppercase;
                }
                .multiButton .buttons {
                    display: flex;
                }
                .multiButton .buttons a {
                    flex-grow: 1;
                }
                .multiButton .buttons a[disabled] {
                    opacity: 0.6;
                    pointer-events: none;
                }
                .multiButton .buttons a.active {
                    border-color: currentColor !important;
                }
                `;

            const $style = jQuery(`<style type="text/css" id="multi-button-style">${styles}</style>`);
            $style.appendTo($head);
        }

        $button.appendTo($menuCore);

        return { button: $button };
    }

    function createButtonStyle(id, iconContent) {
        const styles = `
            #menu-core #${id} a::before {
                ${iconContent ? `content: '${iconContent}'` : ''};
            }
        `;

        const $style = jQuery(`<style type="text/css" id="${id}-style">${styles}</style>`);
        $style.appendTo($head);

        return $style;
    }

    function createDlgFromPassage(passageName, title = passageName) {
        const content = Story.get(passageName).processText();

        Dialog.setup(title);
        Dialog.wiki(content);
        Dialog.open();
    }

    /**
     * Creates button in UI dock opening given passage.
     * @param {string} label Button label
     * @param {string} iconContent Some UTF sequence, like `\\e809\\00a0`
     * @param {string} passageName Passage name to display in dialogue
     * @return {{button: jQuery, style: jQuery}}
     */
    function createPassageButton(label, iconContent, passageName) {
        const id = `menu-item-${passageName}`;

        return {
            button: createButton(id, label, () => createDlgFromPassage(passageName, label)),
            style: createButtonStyle(id, iconContent),
        };
    }

    /**
     * Creates button in UI dock which calls `handler` when clicked.
     * @param {string} label Button label
     * @param {string} iconContent Some UTF sequence, like `\e809\00a0`
     * @param {string} shortName any unique identifier, only letters, digits, dashes, underscore
     * @param {Function} handler Function to call on click/tap
     * @return {{button: jQuery, style: jQuery}}
     */
    function createHandlerButton(label, iconContent, shortName, handler) {
        const id = `menu-item-${shortName}`;

        return {
            button: createButton(id, label, handler),
            style: createButtonStyle(id, iconContent),
        };
    }

    window.scUtils = Object.assign(
        window.scUtils || {},
        {
            createDlgFromPassage,
            createPassageButton,
            createHandlerButton,
            createMultiButton,
        }
    );
}());

(function preloadImagesUtil() {
    'use strict';

    /* globals LoadScreen */

    function preloadImage(source, strict = false) {
        return new Promise((resolve, reject) => {
            const img = new Image();
            img.onload = () => {
                resolve(img);
            };
            if (strict) {
                img.onerror = reject;
            } else {
                img.onerror = () => {
                    resolve(null);
                };
            }

            img.src = source;
        });
    }

    function preloadImages(sources, strict) {
        const lock = LoadScreen.lock();
        const method = (strict ? Promise.all : Promise.any).bind(Promise);
        const promises = sources.map(
            (src) => preloadImage(src, strict)
        );

        return method(promises).finally(() => LoadScreen.unlock(lock));
    }

    window.scUtils = Object.assign(
        window.scUtils || {},
        {
            preloadImages,
        }
    );
}());

scUtils.preloadImages([
    'stage-0.png',
    'stage-1.png',
    'stage-2.png',
    'stage-3.png',
    'not-ending.png',
    'happy-ending.png',
    'logo.png',

    'ring.png',
    'seagull.png',
    'crystal.png',
    'lira.png',
    'dagger.png',
].map(src => `img/${src}`));
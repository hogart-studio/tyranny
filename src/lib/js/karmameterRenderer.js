(function karmameterRenderer() {
    'use strict';

    /* globals jQuery, State */

    function calculateScore(kind, evil) {
        let rate = 0;
        if (evil !== kind) {
            rate = (kind - evil) / (kind + evil);
        }

        return rate;
    }

    function prepareDom() {
        const $karmaMeterWrapper = jQuery('<div class="karmameter-wrapper"></div>');
        const $karmaMeter = jQuery('<div class="karmameter"></div>');
        $karmaMeterWrapper.append($karmaMeter);
        jQuery('#story').prepend($karmaMeterWrapper);
        $karmaMeter.wiki('<<include [[_karmameter]]>>');

        const $kind = $karmaMeter.find('.kind');
        const $evil = $karmaMeter.find('.evil');
        const $scales = $karmaMeter.find('.scales');

        return {
            $kind,
            $evil,
            $scales,
        }
    }

    function renderKarma(score) {
        const opacities = {
            kind: 0,
            evil: 0,
            scales: 0.1,
        };

        if (score < 0) {
            opacities.evil = Math.abs(score);
            opacities.scales = 0;
        } else if (score > 0) {
            opacities.kind = score;
            opacities.scales = 0;
        }

        $kind.css({
            opacity: opacities.kind,
        });
        $evil.css({
            opacity: opacities.evil,
        });
        $scales.css({
            opacity: opacities.scales,
        });
    }

    function getVars() {
        const { kind, evil } = State.current.variables;
        return {
            kind,
            evil,
        };
    }

    const { $kind, $evil, $scales } = prepareDom();

    jQuery(document).on(':passagestart', () => {
        const { kind, evil } = getVars();
        const score = calculateScore(kind, evil);
        renderKarma(score);
    });
}());
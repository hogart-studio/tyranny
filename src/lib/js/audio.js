(function audioFactory() {
    'use strict';
    
    /* globals SimpleAudio */

    const fadeInDuration = 10;
    const crossFadeDuration = 1.6;
    const fadeStopDuration = 1;

    function addTrackstoGroup(group, tracks, path = './sound/') {
        tracks.forEach((trackId) => {
            SimpleAudio.tracks.add(
                trackId,
                path + trackId + '.ogg',
                path + trackId + '.mp3'
            );
            SimpleAudio.groups.add(':' + group, trackId);
        });
    }

    function preloadTracks(tracks, path = './sound/') {
        for (const [group, trackNames] of Object.entries(tracks)) {
            addTrackstoGroup(group, trackNames, path);
        }

        SimpleAudio.loadWithScreen();
    }

    function playCurrentBg(trackId = window.locationFinder.latestLocation) {
        const trackToPlay = SimpleAudio.tracks.get(trackId);

        if (!trackToPlay.isPlaying()) {
            trackToPlay.loop(true);
            trackToPlay.fadeIn(fadeInDuration, 0);
        }
    }

    function fadeCurrentBg(duration = fadeStopDuration) {
        const trackToFade = SimpleAudio.select(':playing:location');
        trackToFade.fadeOut(duration);
        setTimeout(() => {
            trackToFade.stop();
        }, duration * 1000);
    }

    function playGameOver() {
        const gameOverSound = SimpleAudio.select('you-died:ui');
        gameOverSound.play();
        fadeCurrentBg();
    }

    function changeCurrentBg(newTrack) {
        fadeCurrentBg(crossFadeDuration);

        playCurrentBg(newTrack);
    }

    window.myAudio = Object.assign(
        window.myAudio || {},
        {
            preloadTracks,
            playCurrentBg,
            playGameOver,
            changeCurrentBg,
        }
    );
}());